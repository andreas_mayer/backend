package de.awacademy.fomestobackend.comment;


import de.awacademy.fomestobackend.chatroom.ChatroomRepository;
import de.awacademy.fomestobackend.cookEvent.CookEvent;
import de.awacademy.fomestobackend.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class CommentService {

    private CommentRepository commentRepository;
    private ChatroomRepository chatroomRepository;

    @Autowired
    public CommentService(CommentRepository commentRepository, ChatroomRepository chatroomRepository) {
        this.commentRepository = commentRepository;
        this.chatroomRepository = chatroomRepository;
    }

    public Optional<Comment> getComment(long idCookEvent) {

        return commentRepository.findByCookEvent_IdCookEvent(idCookEvent);
    }

    public void saveComment(long idCookEvent) {
        commentRepository.save(commentRepository.findByCookEvent_IdCookEvent(idCookEvent).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden")));
    }

    public void createNewComment(User user, CookEvent cookEvent, String text) {
        Comment comment = new Comment(user, cookEvent);
        comment.setText(text);
        commentRepository.save(comment);
    }

    public Set<Comment> getAllComments() {

        return commentRepository.findAll();
    }
}
