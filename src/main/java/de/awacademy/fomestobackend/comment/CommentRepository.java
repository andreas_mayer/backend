package de.awacademy.fomestobackend.comment;

import de.awacademy.fomestobackend.cookEvent.CookEvent;
import org.springframework.data.repository.CrudRepository;

import java.util.*;

public interface CommentRepository extends CrudRepository<Comment, Long> {

    Set<Comment> findAll();

    Set<Comment> findAllByCookEvent(CookEvent cookEvent);

    Optional<Comment> findAllByIdComment(long id);

    Optional<Comment> findByCookEvent_IdCookEvent(long idCookEvent);

}
