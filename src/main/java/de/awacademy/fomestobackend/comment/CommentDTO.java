package de.awacademy.fomestobackend.comment;

public class CommentDTO {

    // Eigenschaften

    private String text;


    // Konstruktoren

    public CommentDTO(){

    }

    public CommentDTO(String text) {

        this.text = text;
    }


    // Methoden

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
