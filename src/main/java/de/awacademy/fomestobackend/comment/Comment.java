package de.awacademy.fomestobackend.comment;

import de.awacademy.fomestobackend.chatroom.Chatroom;
import de.awacademy.fomestobackend.cookEvent.CookEvent;
import de.awacademy.fomestobackend.user.User;

import javax.persistence.*;

@Entity
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idComment;

    private String text;

    @ManyToOne
    private CookEvent cookEvent;
    @ManyToOne
    private User user;

    @ManyToOne
    private Chatroom chatroom;

    //Konstruktor
    public Comment(){}
    public Comment(String text){
        this.text = text;
    }
    public Comment(User user, CookEvent cookEvent) {
        this.cookEvent = cookEvent;
        this.user = user;
    }

    //Methoden

    public long getIdComment() {
        return idComment;
    }

    public void setIdComment(long idComment) {
        this.idComment = idComment;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public CookEvent getCookEvent() {
        return cookEvent;
    }

    public void setCookEvent(CookEvent cookEvent) {
        this.cookEvent = cookEvent;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Chatroom getChatroom() {
        return chatroom;
    }

    public void setChatroom(Chatroom chatroom) {
        this.chatroom = chatroom;
    }
}
