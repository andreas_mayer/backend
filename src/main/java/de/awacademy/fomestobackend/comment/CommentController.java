package de.awacademy.fomestobackend.comment;


import de.awacademy.fomestobackend.cookEvent.CookEvent;
import de.awacademy.fomestobackend.cookEvent.CookEventDTO;
import de.awacademy.fomestobackend.cookEvent.CookEventService;
import de.awacademy.fomestobackend.security.UserDTO;
import de.awacademy.fomestobackend.user.User;
import de.awacademy.fomestobackend.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Controller
public class CommentController {
    private CommentService commentService;
    private UserService userService;
    private CookEventService cookEventService;

    private UserDTO currentUser;
    private String currentUserName;

    @Autowired
    public CommentController(CommentService commentService, UserService userService, CookEventService cookEventService) {

        this.commentService = commentService;
        this.userService = userService;
        this.cookEventService = cookEventService;
    }

    @GetMapping("/inbox")
    public String showInbox(Model model) {
        cookEventService.getAllCookEvents();
        model.addAttribute("cookEvent", new CookEventDTO());
        model.addAttribute("cookEventlist", cookEventService.getAllCookEvents());
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            this.currentUserName = authentication.getName();
            this.currentUser = new UserDTO(currentUserName);
            model.addAttribute("userLogedIn", currentUser);
        }
        return "inbox";
    }

    @PostMapping("/inbox/annehmen/{username}")
    public String annehmenTeilnahme(Model model, @PathVariable String username) throws UsernameNotFoundException {
        User user = userService.getUser(username).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden"));
        user.setGuest(true);
        userService.saveUser(username);
        return "redirect:/inbox";
    }

    @PostMapping("/inbox/ablehnen/{username}")
    public String ablehnenTeilnahme(Model model, @PathVariable String username) throws UsernameNotFoundException{
        User user = userService.getUser(username).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden"));
        user.setAblehnen(true);
        userService.saveUser(username);
        return "redirect:/inbox";
    }

//    @ModelAttribute("multiCheckboxAllValues")
//    public String[] getMultiCheckboxAllValues() {
//        return new String[] {
//                "Monday", "Tuesday", "Wednesday", "Thursday",
//                "Friday", "Saturday", "Sunday"
//        };
//    }
//
//    @PostMapping("/inbox")
//    public String foobarPost(
//            @ModelAttribute("command") FormCommand command,
//            Model model ) {
//
//    }
//...
}

