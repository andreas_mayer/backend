package de.awacademy.fomestobackend.security;

import de.awacademy.fomestobackend.user.RegistrationDTO;
import de.awacademy.fomestobackend.user.User;
import de.awacademy.fomestobackend.user.UserRepository;
import de.awacademy.fomestobackend.user.UserService;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


@Controller
public class SecurityController {

    @GetMapping("/sessionUser")
    public UserDTO sessionUser(@AuthenticationPrincipal UserDetails userDetails) {
        return new UserDTO(userDetails.getUsername());
    }
}
