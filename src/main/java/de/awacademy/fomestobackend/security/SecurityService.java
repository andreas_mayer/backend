package de.awacademy.fomestobackend.security;

import de.awacademy.fomestobackend.cookEvent.CookEventService;
import de.awacademy.fomestobackend.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import de.awacademy.fomestobackend.user.User;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.LinkedList;
import java.util.List;

@Service
public class SecurityService implements UserDetailsService {

    private PasswordEncoder passwordEncoder;
    private UserService userService;
    private CookEventService cookEventService;




    @Autowired
    public SecurityService(PasswordEncoder passwordEncoder, UserService userService, CookEventService cookEventService) {

        this.passwordEncoder = passwordEncoder;
        this.userService = userService;
        this.cookEventService = cookEventService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {


         User user = userService.getUser(username)
                .orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden"));
        List<GrantedAuthority> authorities = new LinkedList<>();

        if (user.isCook()) {
            authorities.add(new SimpleGrantedAuthority("IS_COOK"));
        }

        return new org.springframework.security.core.userdetails.User(
                user.getUsername(), user.getPassword(), authorities
        );
    }
}
