package de.awacademy.fomestobackend.security;

public class UserDTO {

    private String username;

    public UserDTO(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }
}
