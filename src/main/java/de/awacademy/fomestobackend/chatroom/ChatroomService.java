package de.awacademy.fomestobackend.chatroom;

import de.awacademy.fomestobackend.Entity.EntityService;
import de.awacademy.fomestobackend.comment.Comment;
import de.awacademy.fomestobackend.cookEvent.CookEvent;
import de.awacademy.fomestobackend.user.User;
import org.hibernate.dialect.Cache71Dialect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ChatroomService {
    private  final ChatroomRepository chatroomRepository;
    private final EntityService entityService;

    //Konstruktor
    @Autowired
    public ChatroomService(ChatroomRepository chatroomRepository,EntityService entityService) {
        this.entityService = entityService;
        this.chatroomRepository = chatroomRepository;
    }

    //Methoden
    public Chatroom showChatroom (CookEvent cookEvent) throws UsernameNotFoundException {
        return chatroomRepository.findByCookEvent(cookEvent).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden"));
    }

    public boolean ChatroomExistForCookEvent(CookEvent cookEvent){ return chatroomRepository.existsChatroomByCookEvent(cookEvent);}

    public void newChatroom(CookEvent cookEvent, User user, Comment comment){
    Chatroom chatroom = new Chatroom(cookEvent);
    chatroomRepository.save(chatroom);
    entityService.mapingChatroomCookEvent(chatroomRepository.findByCookEvent(cookEvent).orElseThrow(),cookEvent);
    entityService.mappingChatroomComment(chatroomRepository.findByCookEvent(cookEvent).orElseThrow(),comment);
    entityService.mappingChatroomUser(chatroomRepository.findByCookEvent(cookEvent).orElseThrow(),user);
    }

    public void newComment(Comment comment, Chatroom chatroom){
        entityService.mappingChatroomComment(chatroom,comment);
    }

    public boolean UserExist(User user, CookEvent cookEvent){
        boolean userExist= false;
        for (User userInList :showChatroom(cookEvent).getUsersList()) {
         if(userInList == user){
             userExist= true;
         }else {
             userExist= false;
         }
        }
        return userExist;
    }

    public void addNeuerUser(User user, Chatroom chatroom){
        entityService.mappingChatroomUser(chatroom,user);
    }


}
