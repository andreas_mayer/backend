package de.awacademy.fomestobackend.chatroom;

import de.awacademy.fomestobackend.comment.Comment;
import de.awacademy.fomestobackend.cookEvent.CookEvent;
import org.springframework.data.repository.CrudRepository;
import java.util.*;



public interface ChatroomRepository extends CrudRepository<Chatroom,Long> {
 Set<Chatroom> findAll();
 Optional<Chatroom> findByCookEvent(CookEvent cookEvent);
 boolean existsChatroomByCookEvent(CookEvent cookEvent);
}
