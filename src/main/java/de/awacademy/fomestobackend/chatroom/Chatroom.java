package de.awacademy.fomestobackend.chatroom;

import de.awacademy.fomestobackend.comment.Comment;
import de.awacademy.fomestobackend.cookEvent.CookEvent;
import de.awacademy.fomestobackend.user.User;
import org.hibernate.annotations.ManyToAny;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Set;

@Entity
public class Chatroom {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idChatroom;

    /*ein Chatroom hat viele CommentList von einem User? */
    @OneToMany( mappedBy = "chatroom")
    private Set<Comment> commentsList; //LinkedHashSet

    @OneToMany( mappedBy = "chatroom")
    private Set<User> usersList;

    @OneToOne
    private CookEvent cookEvent;

    //Konstruktor
    public Chatroom(){}

    public Chatroom(CookEvent cookEvent){
        this.cookEvent = cookEvent;

    }

    public Long getIdChatroom() {
        return idChatroom;
    }

    public Set<Comment> getCommentsList() {
        return commentsList;
    }

    public void setCommentsList(LinkedHashSet<Comment> commentsList) {
        this.commentsList = commentsList;
    }

    public Set<User> getUsersList() {
        return usersList;
    }

    public void setUsersList(Set<User> usersList) {
        this.usersList = usersList;
    }

    public CookEvent getCookEvent() {
        return cookEvent;
    }

    public void setCookEvent(CookEvent cookEvent) {
        this.cookEvent = cookEvent;
    }
}
