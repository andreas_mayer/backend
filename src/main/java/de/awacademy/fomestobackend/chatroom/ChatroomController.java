package de.awacademy.fomestobackend.chatroom;

import de.awacademy.fomestobackend.comment.Comment;
import de.awacademy.fomestobackend.comment.CommentDTO;
import de.awacademy.fomestobackend.comment.CommentService;
import de.awacademy.fomestobackend.cookEvent.CookEvent;
import de.awacademy.fomestobackend.cookEvent.CookEventService;
import de.awacademy.fomestobackend.security.UserDTO;
import de.awacademy.fomestobackend.user.User;
import de.awacademy.fomestobackend.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class ChatroomController {
    private ChatroomService chatroomService;
    private UserService userService;
    private CookEventService cookEventService;
    private CommentService commentService;

    @Autowired
    public ChatroomController(CommentService commentService, CookEventService cookEventService, ChatroomService chatroomService, UserService userService){

        this.cookEventService = cookEventService;
        this.chatroomService=chatroomService;
        this.userService = userService;
        this.commentService = commentService;
    }

    @GetMapping("/chatroom/{cookEventId}")
    public String showChatroom(Model model, @PathVariable Long cookEventId) throws UsernameNotFoundException{

        return "index";
    }

    @PostMapping("/chatroom/{cookEventId}")
    public String makeChatroom(Model model, @ModelAttribute ("commentDto") CommentDTO commentDTO, @PathVariable Long cookEventId) throws UsernameNotFoundException{
        User user = userService.getUser(SecurityContextHolder.getContext().getAuthentication().getName()).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden"));
        commentService.createNewComment(user, cookEventService.findCookEvent(cookEventId).orElseThrow(), commentDTO.getText());
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            model.addAttribute("userLogedIn", user);
        }
//        if(!(chatroomService.ChatroomExistForCookEvent(cookEvent) && chatroomService.UserExist(user,cookEvent))){
//            chatroomService.newChatroom(cookEvent,user,commentUser);
//        }else if(chatroomService.ChatroomExistForCookEvent(cookEvent) && !(chatroomService.UserExist(user,cookEvent))){
//            chatroomService.addNeuerUser(user,chatroomService.showChatroom(cookEvent));
//            chatroomService.newComment(commentUser,chatroomService.showChatroom(cookEventService.findCookEvent(cookEventId).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden"))));
//        }else{
//            chatroomService.newComment(commentUser,chatroomService.showChatroom(cookEventService.findCookEvent(cookEventId).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden"))));
//        }
        return "redirect:/";
    }
}
