package de.awacademy.fomestobackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FomestobackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(FomestobackendApplication.class, args);
    }

}
