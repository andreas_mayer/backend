package de.awacademy.fomestobackend.Entity;

import de.awacademy.fomestobackend.chatroom.Chatroom;
import de.awacademy.fomestobackend.comment.Comment;
import de.awacademy.fomestobackend.comment.CommentRepository;
import de.awacademy.fomestobackend.cookEvent.CookEvent;
import de.awacademy.fomestobackend.cookEvent.CookEventRepository;
import de.awacademy.fomestobackend.security.UserDTO;
import de.awacademy.fomestobackend.user.User;
import de.awacademy.fomestobackend.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.LinkedHashSet;
import java.util.Set;

@Service
public class EntityService {
    CookEventRepository cookEventRepository;
    CommentRepository commentRepository;
    UserRepository userRepository;

    @Autowired
    public EntityService(CookEventRepository cookEventRepository, CommentRepository commentRepository, UserRepository userRepository) {
        this.cookEventRepository = cookEventRepository;
        this.commentRepository = commentRepository;
        this.userRepository = userRepository;
    }

    //Mapping User und CookEvent
    public void mapUserCookEvent(UserDTO userDTO, CookEvent cookEvent) throws UsernameNotFoundException {
        userRepository.findByUsernameIgnoreCase(userDTO.getUsername()).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden")).setCook(true);
        userRepository.findByUsernameIgnoreCase(userDTO.getUsername()).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden")).setCookEvent(cookEvent);
        cookEvent.setUser(userRepository.findByUsernameIgnoreCase(userDTO.getUsername()).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden")));
    }

    public void mapUserComment(UserDTO userDTO, Comment comment) throws UsernameNotFoundException{
        userRepository.findByUsernameIgnoreCase(userDTO.getUsername()).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden")).setComment(comment);
        comment.setUser(userRepository.findByUsernameIgnoreCase(userDTO.getUsername()).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden")));
    }

    public void mapCookEventComment(CookEvent cookEvent, Comment comment){
        cookEvent.setCommentList(comment);
        comment.setCookEvent(cookEvent);
    }

    //mapping Chatroom, UserList, CommentList, CookEvent
    public void mapingChatroomCookEvent(Chatroom chatroom, CookEvent cookEvent){
        chatroom.setCookEvent(cookEvent);
        cookEvent.setChatroom(chatroom);
    }
    //mapping UserList Chatroom
    public void mappingChatroomUser (Chatroom chatroom, User user){
        chatroom.getUsersList().add(user);
            user.setChatroom(chatroom);
    }

    //Mapping Chatroom Commentlist
    public void mappingChatroomComment(Chatroom chatroom, Comment comment){
        comment.setChatroom(chatroom);
        LinkedHashSet<Comment> comments = new LinkedHashSet<Comment>();
        comments.add(comment);
        chatroom.setCommentsList(comments);
    }
}

