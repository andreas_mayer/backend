package de.awacademy.fomestobackend.cookEvent;

import de.awacademy.fomestobackend.chatroom.Chatroom;
import de.awacademy.fomestobackend.comment.Comment;
import de.awacademy.fomestobackend.user.User;
import javax.persistence.*;
import java.util.*;

@Entity
public class CookEvent {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idCookEvent;

    @ManyToMany(mappedBy = "cookEvent", cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    private Set<User> user = new HashSet<User>();

    @OneToMany(mappedBy = "cookEvent")
    private Set<Comment> commentList = new HashSet<Comment>();

    @OneToOne
    private Chatroom chatroom;

    private int maxGuest;
    private String title;
    private String date;
    private String descriptionCookEvent;
    private String cook;

    //    Essenseingrenzung:
    private boolean vegan = false;
    private boolean vegy= false;
    private boolean normal = false;

    //Konstruktor

    public CookEvent() {
    }

    public CookEvent(String title, String descriptionCookEvent, String date, int maxGuest, String cook) {
        this.date = date;
        this.title = title;
        this.descriptionCookEvent = descriptionCookEvent;
        this.maxGuest= maxGuest;
        this.cook = cook;
    }

    //Methoden

    public Set<Comment> getCommentList() {
        return commentList;
    }

    public Long getIdCookEvent() {
        return idCookEvent;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescriptionCookEvent() {
        return descriptionCookEvent;
    }

    public void setDescriptionCookEvent(String descriptionCookEvent) {
        this.descriptionCookEvent = descriptionCookEvent;
    }

    public Set<User> getUser() {
        return user;
    }

    public void setUser(User userNew) {
        user.add(userNew);
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setCommentList(Comment comment) {
        this.commentList.add(comment);
    }

    public int getMaxGuest() {
        return maxGuest;
    }

    public void setMaxGuest(int maxGuest) {
        this.maxGuest = maxGuest;
    }

    public String getCook() {
        return cook;
    }

    public void setCook(String cook) {
        this.cook = cook;
    }

    public boolean isVegan() {
        return vegan;
    }

    public void setVegan(boolean vegan) {
        this.vegan = vegan;
    }

    public boolean isVegy() {
        return vegy;
    }

    public void setVegy(boolean vegy) {
        this.vegy = vegy;
    }

    public boolean isNormal() {
        return normal;
    }

    public void setNormal(boolean normal) {
        this.normal = normal;
    }

    public Chatroom getChatroom() {
        return chatroom;
    }

    public void setChatroom(Chatroom chatroom) {
        this.chatroom = chatroom;
    }
}
