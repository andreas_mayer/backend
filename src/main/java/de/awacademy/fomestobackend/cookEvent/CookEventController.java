package de.awacademy.fomestobackend.cookEvent;

import de.awacademy.fomestobackend.Entity.*;
import de.awacademy.fomestobackend.chatroom.ChatroomService;
import de.awacademy.fomestobackend.comment.Comment;
import de.awacademy.fomestobackend.comment.CommentDTO;
import de.awacademy.fomestobackend.comment.CommentService;
import de.awacademy.fomestobackend.security.UserDTO;
import de.awacademy.fomestobackend.user.User;
import de.awacademy.fomestobackend.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class CookEventController {
    private UserService userService;
    private CookEventService cookEventService;
    private ChatroomService chatroomService;
    private CommentService commentService;
    private EntityService entityService;
    private UserDTO currentUser;
    private String currentUserName;

    @Autowired
    public CookEventController(CommentService commentService, CookEventService cookEventService, UserService userService, ChatroomService chatroomService, EntityService entityService) {
        this.userService = userService;
        this.cookEventService = cookEventService;
        this.chatroomService = chatroomService;
        this.entityService = entityService;
        this.commentService = commentService;
    }

    @GetMapping("/")
    public String addCookEvent(Model model) {

        cookEventService.getAllCookEvents();

//        model.addAttribute("chatroom",chatroomService.showChatroom(cookEventService.findCookEvent(cookEventId).orElseThrow(()-> new UsernameNotFoundException("Nicht gefunden"))));
        model.addAttribute("cookEventlist", cookEventService.getAllCookEvents());
        CommentDTO commentDTO = new CommentDTO ();
        model.addAttribute("commentDto", commentDTO);
        model.addAttribute("commentList", commentService.getAllComments());
        // User abfrage Login
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            this.currentUserName = authentication.getName();
            this.currentUser = new UserDTO(currentUserName);
            model.addAttribute("userLogedIn", currentUser);
            User userCurrent = userService.getUser(currentUserName).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden"));
            model.addAttribute("userCurrent", userCurrent);
        }
        return "index";
    }

    @GetMapping("/map")
    public String map(Model model) {
        return "map";
    }

    @GetMapping("/cookEventCreate")
    public String cookEventcreate(Model model) throws UsernameNotFoundException{
        model.addAttribute("cookEvent", new CookEventDTO());
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            this.currentUserName = authentication.getName();
            this.currentUser = new UserDTO(currentUserName);
            model.addAttribute("userLogedIn", currentUser);
        }
        return "cookEventCreate";
    }

    @PostMapping("/cookEventCreate")
    public String addCookEvent(@Valid @ModelAttribute("cookEvent") CookEventDTO cookEventDTO, BindingResult bindingResult) throws UsernameNotFoundException {
        //Validation und BindingResult add
        if (bindingResult.hasErrors()) {
            return "redirect:/";
        }
        CookEvent cookEvent = new CookEvent(cookEventDTO.getTitle(), cookEventDTO.getDescriptionCookEvent(), cookEventDTO.getDate(), cookEventDTO.getMaxGuest(), currentUserName);
        entityService.mapUserCookEvent(currentUser, cookEvent);
        cookEventService.saveCookEvent(cookEvent);
        userService.getUser(currentUserName).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden")).setCook(true);
        return "redirect:/";
    }

    @GetMapping("/cookEvent/{cookEventId}")
    public String showCookEvents(Model model, @PathVariable long cookEventId) throws UsernameNotFoundException {
        CookEvent cookEvent = cookEventService.findCookEvent(cookEventId).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden"));
        model.addAttribute("cookEvent", cookEvent);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            this.currentUserName = authentication.getName();
            this.currentUser = new UserDTO(currentUserName);
            model.addAttribute("userLogedIn", currentUser);
        }
        return "cookEvent";
    }

    // Getmapping
    @GetMapping("/cookEvent/edit/{cookEventId}")
    public String showCookEvent(Model model, @PathVariable long cookEventId) throws UsernameNotFoundException {
        CookEventDTO cookEventDTO = new CookEventDTO(
                cookEventService.findCookEvent(cookEventId).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden")).getTitle(),
                cookEventService.findCookEvent(cookEventId).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden")).getDescriptionCookEvent(),
                cookEventService.findCookEvent(cookEventId).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden")).getDate(),
                cookEventService.findCookEvent(cookEventId).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden")).getMaxGuest(),
                cookEventService.findCookEvent(cookEventId).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden")).getCook(),
                cookEventService.findCookEvent(cookEventId).orElseThrow(()-> new UsernameNotFoundException("Nicht gefunden")).getIdCookEvent());
        model.addAttribute("cookeventEdit", cookEventDTO);

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            this.currentUserName = authentication.getName();
            this.currentUser = new UserDTO(currentUserName);
            model.addAttribute("userLogedIn", currentUser);
        }
        return "cookEventEdit";
    }

    //Post
    @PostMapping("/cookEvent/edit/{cookEventId}")
    public String editCookEvent(Model model, @PathVariable long cookEventId, @ModelAttribute("cookeventEdit") CookEventDTO cookEventDTO) throws UsernameNotFoundException {
        cookEventService.findCookEvent(cookEventId).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden")).setTitle(cookEventDTO.getTitle());
        cookEventService.findCookEvent(cookEventId).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden")).setDescriptionCookEvent(cookEventDTO.getDescriptionCookEvent());
        cookEventService.findCookEvent(cookEventId).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden")).setDate(cookEventDTO.getDate());
        cookEventService.findCookEvent(cookEventId).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden")).setMaxGuest(cookEventDTO.getMaxGuest());
        cookEventService.saveCookEvent(cookEventService.findCookEvent(cookEventId).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden")));
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            this.currentUserName = authentication.getName();
            this.currentUser = new UserDTO(currentUserName);
            model.addAttribute("userLogedIn", currentUser);
        }
        CookEvent cookEvent = cookEventService.findCookEvent(cookEventId).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden"));
        model.addAttribute("cookEvent", cookEvent);
        return "redirect:/";
    }

    @PostMapping("/guestRequest/{cookEventId}")
    public String guestRequest(@PathVariable Long cookEventId) throws UsernameNotFoundException {
        User user = userService.getUser(currentUserName).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden"));
        CookEvent cookEvent = cookEventService.findCookEvent(cookEventId).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden"));
        user.setCookEvent(cookEvent);
        user.setTeilnameAnfrage(true);
        userService.saveUser(currentUserName);
        return "anfrageTeilnahme";
    }

    @GetMapping("/guestRequestSuccess")
    public String showGuestRequest(Model model, @PathVariable Long cookEventId) throws UsernameNotFoundException {
        CookEvent cookEventRequest = cookEventService.findCookEvent(cookEventId).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden"));
        model.addAttribute("cookEventRequest", cookEventRequest);
        return "anfrageTeilnahme";
    }


    /*
    Bug while deleting ->
    >>>>>>> java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row:
     a foreign key constraint fails (`fomesto`.`user_cook_event` CONSTRAINT `FKdunv7ip3u07daglus24bmte2r`
     FOREIGN KEY (`cook_event_id_cook_event`) REFERENCES `cook_event` (`id_cook_event`)) <<<<<<<<

     Vermutung: wegen security fehlt irgendwo ein Token... Vllt beim Getmapping noch ein...
     */

    @GetMapping("/cookEvent/delete/{cookEventId}")
    public String deleteCookEvent (@PathVariable long cookEventId, Model model) throws UsernameNotFoundException{
        CookEvent deleteCookEvent = cookEventService.findCookEvent(cookEventId).orElseThrow(()-> new UsernameNotFoundException("CookEvent nicht gefunden"));
        cookEventService.deleteCookEvent(deleteCookEvent);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            this.currentUserName = authentication.getName();
            this.currentUser = new UserDTO(currentUserName);
            model.addAttribute("userLogedIn", currentUser);
        }
        return "redirect:/";
    }
}

