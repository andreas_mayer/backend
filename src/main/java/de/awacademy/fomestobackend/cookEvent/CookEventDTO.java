package de.awacademy.fomestobackend.cookEvent;

import de.awacademy.fomestobackend.user.User;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

public class CookEventDTO {


    // Eigenschaften

    @NotBlank
    private String title;
    @NotBlank
    private String descriptionCookEvent;
    @NotBlank
    private String date;

    private int maxGuest;


   //Autogeneriert
    private String cook;

    private User user;

    private long id;
    // Konstruktoren

    public CookEventDTO(){

    }

    public CookEventDTO(String title, String descriptionCookEvent, String date,int maxGuest, String cook, long id) {

        this.title = title;
        this.descriptionCookEvent = descriptionCookEvent;
        this.date =date;
        this.maxGuest =maxGuest;
        this.cook = cook;
        this.id = id;
    }


    // Methoden

    public String getTitle() {
        return title;
    }

    public String getDescriptionCookEvent() {
        return descriptionCookEvent;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescriptionCookEvent(String descriptionCookEvent) {
        this.descriptionCookEvent = descriptionCookEvent;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getMaxGuest() {
        return maxGuest;
    }

    public void setMaxGuest(int maxGuest) {
        this.maxGuest = maxGuest;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getCook() {
        return cook;
    }

    public void setCook(String cook) {
        this.cook = cook;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
