package de.awacademy.fomestobackend.cookEvent;


import de.awacademy.fomestobackend.comment.Comment;
import de.awacademy.fomestobackend.comment.CommentRepository;
import de.awacademy.fomestobackend.user.User;
import de.awacademy.fomestobackend.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.Optional;

@Service
public class CookEventService {
    private UserRepository userRepository;
    private CookEventRepository cookEventRepository;
    private CommentRepository commentRepository;

    @Autowired
    public CookEventService(CookEventRepository cookEventRepository, UserRepository userRepository, CommentRepository commentRepository){
        this.userRepository = userRepository;
        this.cookEventRepository = cookEventRepository;
        this.commentRepository = commentRepository;
    }


    public Set<CookEvent> getAllCookEvents(){

        return  cookEventRepository.findAll();
    }

    public void saveCookEvent(CookEvent cookEvent){
        cookEventRepository.save(cookEvent);
    }

    public Optional<CookEvent> findCookEvent(long id){
        return cookEventRepository.findAllByIdCookEvent(id);
    }

    public void deleteCookEvent(CookEvent cookEvent){
        for (User user:userRepository.findAll()) {
        for(CookEvent cookEventToDelete: user.getCookEvent()){
            if(cookEventToDelete == cookEvent){
                user.getCookEvent().remove(cookEvent);
            }
        }
        }
        for (Comment comment:commentRepository.findAll()) {
           if(comment.getCookEvent() == cookEvent){
               commentRepository.delete(comment);
           }
        }
        cookEventRepository.delete(cookEvent);
    }
}
