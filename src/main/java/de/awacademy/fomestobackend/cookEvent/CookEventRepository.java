package de.awacademy.fomestobackend.cookEvent;

import org.springframework.data.repository.CrudRepository;

import java.util.*;
import java.util.Optional;

public interface CookEventRepository extends CrudRepository<CookEvent, Long> {

    Set <CookEvent> findAll(); //Orderby

    Optional<CookEvent> findAllByIdCookEvent(long id);

    CookEvent findByCook(String cookname);
}
