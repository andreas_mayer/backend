package de.awacademy.fomestobackend.user;

import de.awacademy.fomestobackend.cookEvent.CookEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.security.core.*;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder) {

        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public Set<User> getAllUsers(){

        return  userRepository.findAll();
    }

    public boolean usernameExists(String username){

        return userRepository.existsByUsernameIgnoreCase(username);
    }

    public Optional<User> getUser(String username) {

        return userRepository.findByUsernameIgnoreCase(username);
    }

    public Optional<User> getUserById(Long idUser) {

        return userRepository.findById(idUser);
    }

    public void register(String username, String password1, String email,String street, String city, int zipcode,
                         String description) throws UsernameNotFoundException {

        String encodedPassword = passwordEncoder.encode(password1);
        User user = new User(username, encodedPassword);
        user.setEmail(email);
        user.setStreet(street);
        user.setCity(city);
        user.setZipcode(zipcode);
        user.setDescriptionUser(description);
        userRepository.save(user);

    }
    public void saveUser(String username){
        userRepository.save(userRepository.findByUsernameIgnoreCase(username).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden")));
    }

}
