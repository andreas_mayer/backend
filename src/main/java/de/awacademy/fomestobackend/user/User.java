package de.awacademy.fomestobackend.user;

import de.awacademy.fomestobackend.chatroom.Chatroom;
import de.awacademy.fomestobackend.comment.Comment;
import de.awacademy.fomestobackend.cookEvent.CookEvent;

import javax.persistence.*;
import java.util.*;


@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idUser;
    private boolean cook;
    private String username;
    private String password;
    private String email;
    private String descriptionUser;
    private boolean teilnameAnfrage;
    private boolean guest;
    private boolean ablehnen;


    //Adresse:
    private String street;
    private String city;
    private int zipcode;


    @ManyToMany(cascade = {CascadeType.PERSIST,CascadeType.MERGE})
//    @JoinTable(name = "user_cook_event",
//            joinColumns = { @JoinColumn(name = "user_id_user") },
//            inverseJoinColumns = { @JoinColumn(name = "cook_event_id_cook_event")})
    private Set<CookEvent> cookEvent= new TreeSet<>();

    @OneToMany(mappedBy = "user")
    private Set <Comment> commentList = new TreeSet<>();

   @ManyToOne
   private Chatroom chatroom;


    //* Additional: Einbettung Bild -> image


    //Konstruktor
    public User() {
    }

    public User(String userName, String password) {
        this.username = userName;
        this.password = password;
        cook=false;
        teilnameAnfrage = false;
        guest = false;
        ablehnen = false;
    }

    //Methoden
    public Set<CookEvent> getCookEvent() {
        return cookEvent;
    }

    public Set<Comment> getCommentList() {
        return commentList;
    }

    public Long getIdUser() {
        return idUser;
    }

    public boolean isCook() {
        return cook;
    }

    public void setCook(boolean cook) {
        this.cook = cook;
    }

    public Long getId() {
        return idUser;
    }

    public void setId(Long id) {
        this.idUser = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDescriptionUser() {
        return descriptionUser;
    }

    public void setDescriptionUser(String description) {
        this.descriptionUser = description;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getZipcode() {
        return zipcode;
    }

    public void setZipcode(int zipcode) {
        this.zipcode = zipcode;
    }

    public void setCookEvent(CookEvent cookEvent) {
        this.cookEvent.add(cookEvent);
    }
    public  void setComment(Comment comment){
        this.commentList.add(comment);
    }

    public Chatroom getChatroom() {
        return chatroom;
    }

    public void setChatroom(Chatroom chatroom) {
        this.chatroom = chatroom;
    }

    public boolean isTeilnameAnfrage() {
        return teilnameAnfrage;
    }
    public void setTeilnameAnfrage(boolean teilnameAnfrage) {
        this.teilnameAnfrage = teilnameAnfrage;
    }

    public boolean isGuest() {
        return guest;
    }

    public void setGuest(boolean guest) {
        this.guest = guest;
    }

    public boolean isAblehnen() {
        return ablehnen;
    }

    public void setAblehnen(boolean ablehnen) {
        this.ablehnen = ablehnen;
    }
}
