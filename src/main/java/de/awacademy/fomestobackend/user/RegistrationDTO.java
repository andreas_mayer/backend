package de.awacademy.fomestobackend.user;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class RegistrationDTO {


    // Eigenschaften
    @NotBlank
    private final String username;
    @NotBlank
    @Size(min = 5)
    private final String password1;
    private final String password2;
    @NotBlank
    private final String email;
    @NotBlank
    private final String street;
    @NotBlank
    private final String city;
    @NotBlank
    private final String description;

    private final int zipcode;


    // Konstruktoren
    public RegistrationDTO(String username, String password1, String password2,
                           String email,String street, String city, int zipcode, String description) {
        this.username = username;
        this.password1 = password1;
        this.password2 = password2;
        this.email = email;
        this.street =street;
        this.city =city;
        this.zipcode =zipcode;
        this.description = description;
    }

    // Methoden

    public String getUsername() {
        return username;
    }

    public String getPassword1() {
        return password1;
    }

    public String getPassword2() {
        return password2;
    }

    public String getEmail() {
        return email;
    }

    public String getStreet() {
        return street;
    }

    public String getCity() {
        return city;
    }

    public int getZipcode() {
        return zipcode;
    }

    public String getDescription() {
        return description;
    }
}
