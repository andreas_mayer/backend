package de.awacademy.fomestobackend.user;

import org.springframework.data.repository.CrudRepository;

import java.util.*;

public interface UserRepository extends CrudRepository<User, Long> {

    Set<User> findAll();  //OrderBy

    Optional<User> findByUsernameIgnoreCase(String username);

    Optional<User> findIdByUsernameIgnoreCase(String username);

    boolean existsByUsernameIgnoreCase(String username);

    User findByUsernameAndPassword(String username,String password);

    Optional<User> findById(Long id);

}
