package de.awacademy.fomestobackend.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService){

        this.userService = userService;
    }

    @GetMapping("/register")
    public String register(Model model) {
        model.addAttribute("registration", new RegistrationDTO("","","","", "" ,"",0,""));
        return "registration";
    }

    @PostMapping("/register")
    public String registerPost(@Valid @ModelAttribute("registration") RegistrationDTO registrationDTO, BindingResult bindingResult ) {

        if (!registrationDTO.getPassword1().equals(registrationDTO.getPassword2())) {
            bindingResult.addError(new FieldError("registration", "password2", "Stimmt nicht überein"));
        }

        if (bindingResult.hasErrors()) {
            return "registration";
        }
        userService.register(registrationDTO.getUsername(), registrationDTO.getPassword1(),
                registrationDTO.getEmail(), registrationDTO.getStreet(),registrationDTO.getCity(),registrationDTO.getZipcode(), registrationDTO.getDescription());
        return "redirect:/registrationsuccess";
    }

    @GetMapping("/registrationsuccess")
    public String registrationSuccess(){
        return "/registrationsuccess";
    }

}
