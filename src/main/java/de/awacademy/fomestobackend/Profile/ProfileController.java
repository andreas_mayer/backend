package de.awacademy.fomestobackend.Profile;

import de.awacademy.fomestobackend.cookEvent.CookEvent;
import de.awacademy.fomestobackend.cookEvent.CookEventService;
import de.awacademy.fomestobackend.security.UserDTO;
import de.awacademy.fomestobackend.user.RegistrationDTO;
import de.awacademy.fomestobackend.user.User;
import de.awacademy.fomestobackend.user.UserRepository;
import de.awacademy.fomestobackend.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;


@Controller
public class ProfileController {

    private UserService userService;
    private UserDTO currentUser;
    private String currentUserName;


    @Autowired
    public ProfileController(UserService userService) {
        this.userService = userService;

    }


    @GetMapping("/profile/{username}")
    public String showCookEvents(Model model, @PathVariable String username) throws UsernameNotFoundException {

        User user = userService.getUser(username).orElseThrow();
        model.addAttribute("user", user);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            this.currentUserName = authentication.getName();
            this.currentUser = new UserDTO(currentUserName);
            model.addAttribute("userLogedIn", currentUser);
        }
        return "profile";
    }

//Standard GetMapping

//    @GetMapping("/profile/{username}")
//    public String profile(@PathVariable String username, Model model) {
//        User user = userService.getUser(username).orElseThrow();
//        model.addAttribute("user", user);
//        return "profile";
//    }


    @GetMapping("/profile/edit/{username}")
    public String profileBearbeiten(Model model, @PathVariable String username) throws UsernameNotFoundException {
        RegistrationDTO registrationDTO = new RegistrationDTO(
                userService.getUser(username).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden")).getUsername(),
                userService.getUser(username).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden")).getPassword(),
                userService.getUser(username).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden")).getPassword(),
                userService.getUser(username).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden")).getEmail(),
                userService.getUser(username).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden")).getStreet(),
                userService.getUser(username).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden")).getCity(),
                userService.getUser(username).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden")).getZipcode(),
                userService.getUser(username).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden")).getDescriptionUser());
        model.addAttribute("profilZumBearbeiten", registrationDTO);
//        model.addAttribute("profil", userService.getUserById(idUser).orElseThrow());
        return "profileEdit";
    }

    @PostMapping("/profilSpeichern/{username}")
    public String profilSpeichern(@ModelAttribute ("profilZumBearbeiten") RegistrationDTO registrationDTO,
                                   @PathVariable String username) throws UsernameNotFoundException{
        userService.getUser(username).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden")).setEmail(registrationDTO.getEmail());
        userService.getUser(username).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden")).setStreet(registrationDTO.getStreet());
        userService.getUser(username).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden")).setCity(registrationDTO.getCity());
        userService.getUser(username).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden")).setZipcode(registrationDTO.getZipcode());
        userService.getUser(username).orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden")).setDescriptionUser(registrationDTO.getDescription());
        userService.saveUser(username);
        return "profileSubmitted";
    }

}
